const Recipe = require("../models/Recipe");

module.exports.getAll = (req, res) => {
  Recipe.find({}, (err, [data]) => {
    if (err) return res.status(400).json({ error: err.message });
    return res.status(200).json({ data });
  });
};
