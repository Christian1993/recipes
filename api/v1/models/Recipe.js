const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ItemSchema = require("./Item");

const RecipeSchema = new Schema({
  title: {
    type: String
  },
  results: {
    type: [ItemSchema]
  }
});

const Recipe = mongoose.model("recipe", RecipeSchema);
module.exports = Recipe;
