const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Item = new Schema({
  title: {
    type: String,
    required: [true, 'Title field is required']
  },
  href: {
    type: String,
    required: [true, 'Href field is required']
  },
  ingredients: {
    type: String,
    required: [true, 'Ingredients field is required']
  },
  thumbnail: {
    type: String,
    required: [true, 'Thumbnail field is required']
  },
  
})

module.exports = Item;
