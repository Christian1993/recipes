const express = require("express");
const mongoose = require("mongoose");
const config = require("config");

const corsHandler = require("./api/v1/middleware/cors-handler");

const pagesRouter = require("./api/v1/routes/pages");
const recipesRouter = require("./api/v1/routes/recipes");

const app = express();
const db = config.get("mongoUrl");

mongoose
  .connect(db, {
    useNewUrlParser: true
  })
  .then(() => console.log("Connection established"))
  .catch(err => console.log(err));

app.use(express.json());
app.use(express.static(__dirname + "/client/build"));
app.use(corsHandler.cors_handler);

app.use(pagesRouter);
app.use("/api/v1/recipes", recipesRouter);

module.exports = app;
