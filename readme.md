# Recipes
Application allows to get couple recipes and filter them. Sipmple API is built with node, express and mongodb. 

#### Technologies used:
* react,
* redux,
* reactstrap,
* axios,
* sass,
* mongodb,
* express

#### Live:
[Live](https://enigmatic-woodland-77487.herokuapp.com/)
