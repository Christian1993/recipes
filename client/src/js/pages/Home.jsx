import React, { Component } from "react";
import { connect } from "react-redux";
import { getRecipes } from "../store/actions/recipesActions";
import { Container } from "reactstrap";
import Recipes from "../components/Recipes";
import SearchBox from "../components/search/SearchBox";

class Home extends Component {
  componentDidMount() {
    this.props.getRecipes();
  }
  render() {
    return (
      <div className="home">
        <Container>
          <SearchBox />
          <Recipes />
        </Container>
      </div>
    );
  }
}

export default connect(
  null,
  { getRecipes }
)(Home);
