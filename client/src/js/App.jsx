import React, { Component } from "react";
import Home from "./pages/Home";
import { Provider } from "react-redux";
import store from "./store/store";
import Header from "./layout/Header";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Header />
        <Home />
      </Provider>
    );
  }
}
