import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";

export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <Container>
          <Row>
            <Col>
              <h1>Search for a recipe</h1>
            </Col>
          </Row>
        </Container>
      </header>
    );
  }
}
