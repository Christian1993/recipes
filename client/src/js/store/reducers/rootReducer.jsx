import { combineReducers } from "redux";
import recipesReducer from "./recipesReducer";
import searchReducer from "./searchReducer";

const rootReducer = combineReducers({
  recipes: recipesReducer,
  manually: searchReducer
});

export default rootReducer;
