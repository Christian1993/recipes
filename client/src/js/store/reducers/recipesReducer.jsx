import { GET_RECIPES, FILTER_RECIPES } from "../actions/types";

const initialState = {
  recipes: [],
  visibleRecipes: []
};
export default function todosReducer(state = initialState, action) {
  switch (action.type) {
    case GET_RECIPES: {
      return {
        ...state,
        recipes: action.payload
      };
    }
    case FILTER_RECIPES: {
      return {
        ...state,
        visibleRecipes: action.payload
      };
    }
    default: {
      return state;
    }
  }
}
