import { SET_SEARCH } from "../actions/types";

const initialState = {
  manually: false
};
export default function todosReducer(state = initialState, action) {
  switch (action.type) {
    case SET_SEARCH: {
      return {
        ...state,
        manually: action.payload
      };
    }
    default: {
      return state;
    }
  }
}
