import axios from "axios";
import { GET_RECIPES, FILTER_RECIPES } from "./types";

export const getRecipes = () => dispatch => {
  axios
    .get("/api/v1/recipes")
    .then(({ data }) =>
      dispatch({
        type: GET_RECIPES,
        payload: data.data.results
      })
    )
    .catch(err => alert("Something gone wrong, please try later."));
};

export const filterRecipes = tags => (dispatch, getState) => {
  const allRecipes = getState().recipes.recipes;
  const filtered = allRecipes.filter(recipe =>
    tags.some(tag => recipe.ingredients.split(", ").includes(tag))
  );
  dispatch({
    type: FILTER_RECIPES,
    payload: filtered
  });
};
