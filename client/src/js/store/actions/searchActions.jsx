import { SET_SEARCH } from "./types";

export const setSearch = val => dispatch => {
  dispatch({
    type: SET_SEARCH,
    payload: val
  });
};
