export const GET_RECIPES = "GET_RECIPES";
export const FILTER_RECIPES = "FILTER_RECIPES";

export const SET_SEARCH = "SET_SEARCH";

