import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";

export default class Recipe extends Component {
  render() {
    const { title, href, thumbnail, ingredients } = this.props.recipe;
    return (
      <Col sm={4} lg={3}>
        <div className="recipe">
          <Card>
            <CardImg top width="100%" src={thumbnail} alt={title} />
            <CardBody>
              <CardTitle>
                <strong>{title}</strong>
              </CardTitle>
              <CardSubtitle>{ingredients}</CardSubtitle>
              <a className="link" href={href} target="blank">
                Orginal recipe
              </a>
            </CardBody>
          </Card>
        </div>
      </Col>
    );
  }
}

Recipe.propTypes = {
  recipe: PropTypes.shape({
    title: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    thumbnail: PropTypes.string.isRequired,
    ingredients: PropTypes.string.isRequired
  }).isRequired
};
