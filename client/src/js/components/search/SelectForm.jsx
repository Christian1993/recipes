import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { filterRecipes } from "../../store/actions/recipesActions";

class SelectForm extends Component {
  state = {
    tags: []
  };
  handleChange = e => {
    const selected = Array.from(e.target.selectedOptions, el => el.value);
    this.setState({
      tags: selected
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.filterRecipes(this.state.tags);
  };

  renderOptions = recipes => {
    const ingredientsArr = recipes
      .reduce((acc, recipe) => {
        return acc + recipe.ingredients;
      }, "")
      .split(", ")
      .sort();
    const ingredients = [...new Set(ingredientsArr)];
    return ingredients.map(ingredient => (
      <option key={ingredient} value={ingredient}>
        {ingredient}
      </option>
    ));
  };

  render() {
    const { recipes } = this.props;
    if (!recipes.length) return <div>Loading...</div>;
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="selectTags">
              You can select multiple ingredientst (Ctrl + click)
            </Label>
            <Input
              type="select"
              name="selectTags"
              id="selectTags"
              multiple
              onChange={this.handleChange}
            >
              {this.renderOptions(recipes)}
            </Input>
          </FormGroup>

          <Button>Search</Button>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    recipes: state.recipes.recipes
  };
};

SelectForm.propTypes = {
  recipes: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      href: PropTypes.string.isRequired,
      ingredients: PropTypes.string.isRequired,
      thumbnail: PropTypes.string.isRequired
    })
  ).isRequired,
  filterRecipes: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  { filterRecipes }
)(SelectForm);
