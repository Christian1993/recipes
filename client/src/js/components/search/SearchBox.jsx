import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Row, Col } from "reactstrap";
import { setSearch } from "../../store/actions/searchActions";
import ManualForm from "./ManualForm";
import SelectForm from "./SelectForm";

class SearchBox extends Component {
  render() {
    const { manually } = this.props;
    const search = manually ? <ManualForm /> : <SelectForm />;
    return (
      <div className="searchBox">
        <Row>
          <Col>{search}</Col>
        </Row>
        <Row>
          <Col>
            <button
              className="link"
              onClick={() => this.props.setSearch(!manually)}
            >
              Toggle search method
            </button>
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    manually: state.manually.manually
  };
};

export default connect(
  mapStateToProps,
  { setSearch }
)(SearchBox);

SearchBox.propTypes = {
  manually: PropTypes.bool.isRequired,
  setSearch: PropTypes.func.isRequired
};
