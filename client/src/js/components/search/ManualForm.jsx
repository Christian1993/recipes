import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { filterRecipes } from "../../store/actions/recipesActions";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

class ManualForm extends Component {
  state = {
    tags: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const tags = this.state.tags.split(",");
    this.props.filterRecipes(tags);
  };
  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="tags">Type ingredients</Label>
            <Input
              type="text"
              name="tags"
              id="tags"
              placeholder="E.g. eggs, onions, olive"
              value={this.state.tags}
              onChange={this.handleChange}
            />
          </FormGroup>
          <Button>Search</Button>
        </Form>
      </div>
    );
  }
}

ManualForm.propTypes = {
  filterRecipes: PropTypes.func.isRequired
};

export default connect(
  null,
  { filterRecipes }
)(ManualForm);
