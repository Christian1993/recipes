import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Row } from "reactstrap";
import Recipe from "./Recipe";

class Recipes extends Component {
  render() {
    return (
      <Row>
        {this.props.recipes &&
          this.props.recipes.map((recipe, i) => {
            return <Recipe key={i} recipe={recipe} />;
          })}
      </Row>
    );
  }
}

const mapStateToProps = state => {
  return {
    recipes: state.recipes.visibleRecipes
  };
};
Recipes.propTypes = {
  recipes: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      href: PropTypes.string.isRequired,
      ingredients: PropTypes.string.isRequired,
      thumbnail: PropTypes.string.isRequired
    })
  ).isRequired
};

export default connect(mapStateToProps)(Recipes);
