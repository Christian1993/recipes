import React from "react";
import ReactDOM from "react-dom";
import App from "./js/App.jsx";

import "bootstrap/dist/css/bootstrap.css";
import "./sass/main.sass";

ReactDOM.render(<App />, document.getElementById("root"));
